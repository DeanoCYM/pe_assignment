clc; clear all;

%% Get user inputs for X, Y, F and h
% X - size of plate on the x-axis
% Y - size of plate on the y-axis
% F - Fourier number
% h - grid spacing

% Two dimensions define the plate, use sensible defaults if no
% input provided

X = input('Enter dimension of plate along x-axis (default=6): ');

if isempty(X)
    X = 6;                             % default value
    fprintf('Default value of %.0f used\n',X);
end

Y = input('Enter dimension of plate along y-axis (default=9): ');

if isempty(Y)
    Y = 9;                             % default value
    fprintf('Default value of %.0f used\n',Y);
end

% Fourier number required to be greater than 0 but less than 0.25,
% this is the stability criteria

limits_F = [ 0, 0.25 ];                % limits for stability
F = input('Enter a value for the Fourier number (default=0.15): ');

if isempty(F)
    F = 0.15;                           % default value
    fprintf('Default value of %.2f used\n',F);
end

while ( F <= limits_F(1) || F > limits_F(2) ) % check limits
    warning('Fourier number must satisfy: 0 <= F < 0.25');
    F = input('Enter a new value for the Fourier number: ');
end

% Grid spacing must divide into the x and y dimensions without a
% remainder so that nodes are equidistant
h = input('Enter required grid spacing (default=3): ');

if isempty(h)
    h = 3;                           % default value
    fprintf('Default value of %.0f used\n',h);
end

while ( rem(X, h) ~= 0 || rem(Y, h) ~= 0 ) % ensure no remainders
    warning('Grid spacing does not comply with plate dimensions');
    h = input('Enter required grid spacing: ');
end

%% Generation of the grid
% u  - grid which will contain plate temperatures at each node
%      this is a 3D matrix that is repeated for each timestep
%      grid will also contain the boundry temperatures at the plate edge
% xx - number of nodes in x direction
% yy - number of nodes in y direction
% N  - maximum number of iterations, each iteration is a timestep

N = 1000;               % should be more than enough to converge on solution
xx = 2 + (X / h);       % needs to be +2 for boundries to be included
yy = 2 + (Y / h);       % as above

% Note: matlab matricies count from 1 not 0.
%       matlab has the first parameter as the row (y-axis)
%       and the second as the column (x-axis)
%
% Orign taken as top left of plate [x,y,n] = [0,0,0]
% in a matlab in a matrix this is equivilent to u(y,x,1) = u(1,1,1) 
% 
% Similarly, the first timestep is at n = 1 not n = 0.

u = zeros(yy, xx, N);   % generate N empty plate matrices for each timestep

%% Boundary/Initial Conditions
% T_initial - initial temperature of plate (degC)
% T_b       - vector of temperatures at boundaries (NSEW)
%
% x - x co-ordinates (origin at x=0)
% y - y co-ordinates (origin at y=0)

T_initial = 10;
u(:,:,1) = T_initial;

% Define inline functions for the boundary conditions
%
% model using quadratic equation:
%     Temperature = ax^2 + bx + c         (y is used if on y-axis)
% if linear:   a = 0
% if constant: a = b = 0
%
% north and south are on x-axis, east and west on y axis

north = @(x) (0*x.^2) +  x*((50-50)/(X-0)) + 50;
south = @(x) (0*x.^2) +  x*((50-50)/(X-0)) + 10;
east  = @(y) (0*y.^2) +  y*((10-50)/(Y-0)) + 50;
west  = @(y) (0*y.^2) +  y*((10-50)/(Y-0)) + 50;

% Determine boundry temperatures using the inline functions

T_b_north = north( linspace(0,X,xx) );
T_b_south = south( linspace(0,X,xx) );
T_b_east  = east(  linspace(0,Y,yy) );
T_b_west  = west(  linspace(0,Y,yy) );

% The boundry conditions do not change with time and so
% boundry vectors can be applied to each timestep in u matrix
% matricies are duplicated with the function repmat()

u(  1  ,  :  , : ) = repmat( T_b_north, [ 1 1 N ] );
u( end ,  :  , : ) = repmat( T_b_south, [ 1 1 N ] );
u(  :  ,  1  , : ) = repmat( T_b_east,  [ 1 1 N ] );
u(  :  , end , : ) = repmat( T_b_west,  [ 1 1 N ] );

%% Convergence criteria
% epsilon_tar  - target convergence
%                stop iterating when target = actual convergence
% epsilon_act  - actual convergence
%                determined by taking difference of temperatures at
%                time n from time n + 1

epsilon_tar = 0.001;
epsilon_act = 100;      % just initialise, will be calculated later

%% Iterations
% n - timestep (n = 1 is initial, n + 1 is solved iteratively using
%     heat equation below:
%
%     Tnn = F(Tn + Ts + Te + Tw - Tp(4 - (1/F)))
%         = F(Tsum - Tp(4 - (1/F)))
%
% Tn - temperature at time n at node + grid spacing h in north direction
% Ts - temperature at time n at node + grid spacing h in south direction
% Te - temperature at time n at node + grid spacing h in east direction
% Tw - temperature at time n at node + grid spacing h in west direction

% T_sum - sum of all NSEW temperatures (used in heat equation)
% T_p   - Temperature at node at previous timestep
% T     - Temperature at node at timestep to be calculated

% skip first timestep (n = 1) already calculated from boundary/initial
% conditions so move on to n = 2

n = 2;

% Iterate heat equation until convergence criterion satisfied or
% maximum number of iterations reached (N)

while n <= N && epsilon_act > epsilon_tar

    % Perform for each node in the array except the nodes at the edge,
    % which are already defined as the boundary conditions
    
    for x = 2 : xx - 1                  % iterate along x-axis
        for y = 2 : yy - 1              % iterate along y-axis

            Tn = u(  y  , x-1 , n-1 );  % temp north of point at previous timestep
            Ts = u(  y  , x+1 , n-1 );  % temp south of point at previous timestep
            Te = u( y+1 ,  x  , n-1 );  % temp east of point at previous timestep
            Tw = u( y-1 ,  x  , n-1 );  % temp west of point at previous timestep

            Tsum = Tn + Te + Ts + Tw;

            Tp = u(  y  ,  x  , n-1 );  % temp at previous timestep

            % Use heat equation to determine temp at current timestep
            T = F*( Tsum - Tp*(4 - (1/F)));

            % Assign calculated temperature to array
            u(y,x,n) = T;

        end
    end
    
    % compare current plate to previous plate to check for
    % convergence, absolute maximum change is recorded

    change = u(:,:,n) - u(:,:,n-1);
    epsilon_act = max(abs(change(:)));
    
    n = n + 1;                          % move onto next timestep

end

%% Trim the excess elements from the array and display results
% If the maximum number of iterations was reached, then there was no
% convergence

if n < N      % convergence met

    % n-1 timestep is timestep where corvergance was met
    % n : N timesteps can be removed as they were not filled
    u = u(:, :, 1 : (n-1) );

    % display convergance time in terminal
    fprintf(['Converged in %.0f iterations\n', ...
            'Equilibrium temperatures:\n'], n - 1);

    if size(u,1) <= 20 && size(u,2) <= 10

        % Show the equilibrium temperature (n-1)
        disp(u(:,:,n-1));

    else

        % Dont display result in terminal if matrix is large
        disp('Too large to display here!');

    end
    
else
    
    % if this error is reached a solution has not been found
    %     - increase max iterations (N)
    %     - increase the Fourier number (F)
    %     - decrease the convergacnce requirement (epsilon_tar)

    fprintf( ['Maximum iterations reached with no covergance', ...
              ' after %.0f iterations\n\nDifference of ',      ...
              '%.2e was present between timesteps n and n+1',  ...
              '\nTarget convergence is %.2e\n\n'],             ...
             n-1, epsilon_act, epsilon_tar )

    error('Did not converge, increase maximum iterations');
    
end
